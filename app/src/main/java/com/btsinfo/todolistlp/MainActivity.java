package com.btsinfo.todolistlp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Tache>listeTache;
    private Button btAddT;
    private ListView lvTache;
    private int currentListItemIndex;
    private ListAdapter listeAdapter;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listeTache = new DatabaseManager(this).LectureTache();

        context = this;
        btAddT = (Button) findViewById(R.id.btAddT);
        lvTache = (ListView)findViewById(R.id.ListView1);

        btAddT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startViewActivityAdd();
            }
        });


        lvTache.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                currentListItemIndex = position;
                currentActionMode = startActionMode(modeCallBack);
                view.setSelected(true);
                return true;
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        listeAdapter = new ListAdapter(this,listeTache);
        lvTache.setAdapter(listeAdapter);
    }

    private ActionMode currentActionMode;
    private ActionMode.Callback modeCallBack = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Edit");
            mode.getMenuInflater().inflate(R.menu.barre_action, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete:
                    Tache suppTache = listeTache.get(currentListItemIndex);
                    int idTD = suppTache.getIdT();
                    new DatabaseManager(context).DeleteTache(idTD);
                    listeAdapter.remove(suppTache);
                    listeAdapter.notifyDataSetChanged();
                    mode.finish();
                    return true;
                case R.id.menu_edit:
                    startViewActivityMod(currentListItemIndex);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            currentActionMode = null;
        }
    };

    private void startViewActivityAdd()
    {
        Intent intent = new Intent (this,AjoutActivity.class);

        startActivity(intent);
    }
    private void startViewActivityMod(int i)
    {
        Tache uneTache = listeTache.get(i);
        Intent intent = new Intent(this,ModifActivity.class);
        intent.putExtra("idT",uneTache.getIdT());

        startActivity(intent);
    }


}
