package com.btsinfo.todolistlp;

import java.util.Date;

public class Tache {
    int idT;
    String TitreT;
    String DescT;
    String DateCreaT;
    String DateFinT;
    int PrioT;


    public Tache (int idT, String titreT, String descT, String dateCreaT, String dateFinT, int prioT)
    {
        this.idT = idT;
        TitreT = titreT;
        DescT= descT;
        DateCreaT = dateCreaT;
        DateFinT = dateFinT;
        PrioT = prioT;
    }

    public int getIdT() {
        return idT;
    }

    public void setIdT(int idT) {
        this.idT = idT;
    }

    public String getTitreT() {
        return TitreT;
    }

    public void setTitreT(String titreT) {
        TitreT = titreT;
    }

    public String getDescT() {
        return DescT;
    }

    public void setDescT(String descT) {
        DescT = descT;
    }

    public String getDateCreaT() {
        return DateCreaT;
    }

    public void setDateCreaT(String dateCreaT) {
        DateCreaT = dateCreaT;
    }

    public String getDateFinT() {
        return DateFinT;
    }

    public void setDateFinT(String dateFinT) {
        DateFinT = dateFinT;
    }

    public int getPrioT() {
        return PrioT;
    }

    public void setPrioT(int prioT) {
        PrioT = prioT;
    }
}
