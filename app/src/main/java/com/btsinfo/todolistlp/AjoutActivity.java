package com.btsinfo.todolistlp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AjoutActivity extends AppCompatActivity {

    private EditText etNomT;
    private EditText etDescT;
    private Spinner spAjoutPrio;
    private Button btAjoutT,btCloseA;
    private ArrayList<String> listePrio;
    private int choixPrio;
    private String dateCrea,dateFin;
    private DatePicker dpAdd;

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);

        etNomT = (EditText) findViewById(R.id.etNomT);
        etDescT = (EditText) findViewById(R.id.etDescT);
        dpAdd = (DatePicker) findViewById(R.id.dpAdd);
        spAjoutPrio = (Spinner) findViewById(R.id.spAjoutPrio);
        btAjoutT = (Button) findViewById(R.id.btAjoutT);
        btCloseA = (Button) findViewById(R.id.btCloseA);

        context = this;
        listePrio = new DatabaseManager(this).getNomPrio();
        ArrayAdapter<String> adapterF = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listePrio);
        adapterF.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAjoutPrio.setAdapter(adapterF);
        spAjoutPrio.setSelection(3);

        spAjoutPrio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixPrio = adapterView.getSelectedItemPosition()+1;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        String date = sdf.format(new Date());
        dateCrea = date;

        dateFin= date;
        dpAdd.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                Calendar calendar = new GregorianCalendar(dpAdd.getYear(),
                        dpAdd.getMonth(),
                        dpAdd.getDayOfMonth());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                dateFin = sdf.format(calendar.getTime());
            }
        });





        btAjoutT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatabaseManager(context).insertTache(etNomT.getText().toString(),etDescT.getText().toString(),dateCrea,dateFin,choixPrio);
                finish();
                toMain();
            }
        });

        btCloseA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                toMain();

            }
        });

    }
    public void toMain()
    {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}


