package com.btsinfo.todolistlp;

public class Priorite {

    int idP;
    String NomP;
    String ImgP;


    public Priorite(int idP, String nomP, String imgP)
    {
        this.idP = idP;
        NomP = nomP;
        ImgP = imgP;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNomP() {
        return NomP;
    }

    public void setNomP(String nomP) {
        NomP = nomP;
    }

    public String getImgP() {
        return ImgP;
    }

    public void setImgP(String imgP) {
        ImgP = imgP;
    }
}