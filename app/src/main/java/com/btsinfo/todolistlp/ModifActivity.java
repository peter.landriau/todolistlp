package com.btsinfo.todolistlp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ModifActivity extends AppCompatActivity {

    private TextView tvNomT;
    private EditText etDescT;
    private Spinner spModPrio;
    private Button btModifT,btCloseM;
    private ArrayList<String> listePrio;
    private int choixPrio;
    private String dateFin;
    private DatePicker dpMod;
    private Tache laTache;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif);

        tvNomT = (TextView) findViewById(R.id.tvNomT);
        etDescT = (EditText) findViewById(R.id.etDescT);
        dpMod = (DatePicker) findViewById(R.id.dpMod);
        spModPrio = (Spinner) findViewById(R.id.spModPrio);
        btModifT = (Button) findViewById(R.id.btModifT);
        btCloseM = (Button) findViewById(R.id.btCloseM);

        final Intent rIntent = getIntent();
        final int idT = rIntent.getIntExtra("idT",0);
        laTache = new DatabaseManager(this).LectureTache(idT);
        context = this;

        tvNomT.setText(laTache.getTitreT());
        etDescT.setText(laTache.getDescT());

        listePrio = new DatabaseManager(this).getNomPrio();
        ArrayAdapter<String> adapterF = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listePrio);
        adapterF.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spModPrio.setAdapter(adapterF);
        spModPrio.setSelection(laTache.getPrioT()-1);

        spModPrio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixPrio = adapterView.getSelectedItemPosition()+1;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Date fin
        String dateFS = laTache.getDateFinT();
        String[] dateFSplit = dateFS.split("/");

        dpMod.updateDate(Integer.parseInt(dateFSplit[2]),Integer.parseInt(dateFSplit[1])-1,Integer.parseInt(dateFSplit[0]));
        dpMod.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                Calendar calendar = new GregorianCalendar(dpMod.getYear(),
                        dpMod.getMonth(),
                        dpMod.getDayOfMonth());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
                dateFin = sdf.format(calendar.getTime());
            }
        });





        btModifT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new DatabaseManager(context).UpdateTache(idT,etDescT.getText().toString(),dateFin,choixPrio);
                finish();
                toMain();
            }
        });

        btCloseM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                toMain();

            }
        });
    }
    public void toMain()
    {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
