package com.btsinfo.todolistlp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;


public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="todo.db";
    private static final int DATABASE_VERSION = 3;

    public DatabaseManager(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String strSqlT="create table Tache(idT INTEGER primary key AUTOINCREMENT, titreT String, descT String, dateCreaT String, dateFinT String, prioT int)";
        db.execSQL(strSqlT);
        String strSqlP="create table Priorite(idP int primary key, nomP String, imgP String)";
        db.execSQL(strSqlP);

        //Insertion valeurs
        String p1 = "insert into Priorite values (1,'Critique','#FF0000')";
        String p2 = "insert into Priorite values (2,'Très urgent','#FF8000')";
        String p3 = "insert into Priorite values (3,'Urgent','#FFFF00')";
        String p4 = "insert into Priorite values (4,'Non urgent','#00CC00')";


        //Execution requêtes
        db.execSQL(p1);
        db.execSQL(p2);
        db.execSQL(p3);
        db.execSQL(p4);

        Log.i("DATABASE","onCreate invoqué");



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        String strSqlT="drop table Tache";
        db.execSQL(strSqlT);
        String strSqlP="drop table Priorite";
        db.execSQL(strSqlP);

        this.onCreate(db);

    }

    //Insertion d'une Tâche
    public void insertTache(String titreT , String descT, String dateCreaT, String dateFinT, int prioT)
    {
        String sql = "insert into Tache(titreT,descT,dateCreaT,dateFinT,prioT) values ('"+titreT+"','"+descT+"','"+dateCreaT+"','"+dateFinT+"','"+prioT+"')";
        this.getWritableDatabase().execSQL(sql);
    }
    //MaJ d'une Tâche
    public void UpdateTache(int idT , String descT, String dateFinT, int prioT)
    {
        String sql = "Update Tache set descT = '"+descT+"',dateFinT = '"+dateFinT+"',prioT = '"+prioT+"' where idT = '"+idT+"'";
        this.getWritableDatabase().execSQL(sql);
    }

    //Suppression d'une Tâche
    public void DeleteTache(int idT)
    {
        String sql = "Delete from Tache where idT = '"+idT+"'";
        this.getWritableDatabase().execSQL(sql);
    }

    //Lecture d'une priorité choisie
    public Priorite LecturePrio(int idP)
    {

        String sqlP = "select * from Priorite where idP ="+idP;
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP,null);
        curseur.moveToFirst();
        Priorite lapriorite = new Priorite(curseur.getInt(0),curseur.getString(1),curseur.getString(2));
        curseur.close();
        return  lapriorite;
    }

    //Get du nom de la priorité
    public ArrayList<String> getNomPrio()
    {
        ArrayList<String> nomPriorites= new ArrayList<>();
        String sqlP = "select * from Priorite";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP,null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            String nomP = curseur.getString(curseur.getColumnIndex("nomP"));
            nomPriorites.add(nomP);
            curseur.moveToNext();
        }
        curseur.close();
        return  nomPriorites;
    }

    //Lecture des tâches
    public ArrayList<Tache> LectureTache()
    {
        ArrayList<Tache> taches= new ArrayList<>();
        String sqlP = "select * from Tache order by prioT asc, dateFinT asc";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP,null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Tache tache = new Tache(curseur.getInt(0),curseur.getString(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getInt(5));
            taches.add(tache);
            curseur.moveToNext();
        }
        curseur.close();
        return  taches;
    }

    //Lecture des tâches
    public Tache LectureTache(int idT)
    {
        String sqlP = "select * from Tache where idT="+idT+" order by prioT asc, dateFinT asc";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP,null);
        curseur.moveToFirst();
        Tache latache = new Tache(curseur.getInt(0),curseur.getString(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getInt(5));
        curseur.close();
        return  latache;
    }
}