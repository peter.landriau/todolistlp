package com.btsinfo.todolistlp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class ListAdapter extends ArrayAdapter<Tache> {

    Context context;
    long ecartDate;


    public ListAdapter(Context context, List<Tache> listeTache)
    {
        super(context, -1, listeTache);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        Tache uneTache;
        Priorite laPriorite;
        view = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.ligne, parent, false);

        } else {

            view = convertView;

        }

        uneTache = getItem(position);
        TextView tvTitreT = (TextView) view.findViewById(R.id.Titre1);
        TextView tvDateT = (TextView) view.findViewById(R.id.Titre2);
        TextView tvDescT = (TextView) view.findViewById(R.id.Titre3);
        ImageView imageView = (ImageView) view.findViewById(R.id.imgP);

        tvTitreT.setText(uneTache.getTitreT());
        tvDateT.setText(uneTache.getDateFinT());
        tvDescT.setText(uneTache.getDescT());
        int idP = uneTache.getPrioT();


        //Evolution priorité

        //Date création
        String dateCS = uneTache.getDateCreaT();
        String[] dateCSplit = dateCS.split("/");
        Date dateCD = new GregorianCalendar(Integer.parseInt(dateCSplit[2]),Integer.parseInt(dateCSplit[1]),Integer.parseInt(dateCSplit[0])).getTime();

        //Date fin
        String dateFS = uneTache.getDateFinT();
        String[] dateFSplit = dateFS.split("/");
        Date dateFD = new GregorianCalendar(Integer.parseInt(dateFSplit[2]),Integer.parseInt(dateFSplit[1]),Integer.parseInt(dateFSplit[0])).getTime();

        //Date actuelle
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        String dateAS = sdf.format(new Date());
        String[] dateASplit = dateAS.split("/");
        Date dateAD = new GregorianCalendar(Integer.parseInt(dateASplit[2]),Integer.parseInt(dateASplit[1]),Integer.parseInt(dateASplit[0])).getTime();

        //Différence entre les dates Fin et Crea
        long diffDateFC = dateFD.getTime() - dateCD.getTime();

        //Différence entre les dates Fin et Actuelle
        long diffDateFA = dateFD.getTime() - dateAD.getTime();

        //Cas avec 4 prio
        if(idP == 4)
        {

            ecartDate =(diffDateFC/4);
            if((ecartDate*3)<diffDateFA && diffDateFA<=diffDateFC)
            {
                laPriorite = new DatabaseManager(context).LecturePrio(4);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));

            }
            if((ecartDate*2)<diffDateFA && diffDateFA<=(ecartDate*3))
            {
                laPriorite = new DatabaseManager(context).LecturePrio(3);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));

            }
            if(ecartDate<diffDateFA && diffDateFA<=(ecartDate*2))
            {
                laPriorite = new DatabaseManager(context).LecturePrio(2);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));

            }
            if(diffDateFA<=ecartDate)
            {
                laPriorite = new DatabaseManager(context).LecturePrio(1);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));

            }
        }

        //Cas avec 3 prio
        if(idP == 3)
        {

            ecartDate = (diffDateFC/3);

            if((ecartDate*2)<diffDateFA && diffDateFA<=diffDateFC)
            {
                laPriorite = new DatabaseManager(context).LecturePrio(3);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));
            }
            if(ecartDate<diffDateFA && diffDateFA<=(ecartDate*2))
            {
                laPriorite = new DatabaseManager(context).LecturePrio(2);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));
            }
            if(diffDateFA<=ecartDate)
            {
                laPriorite = new DatabaseManager(context).LecturePrio(1);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));
            }
        }

        //Cas avec 2 prio
        if(idP == 2) {

            ecartDate = (diffDateFC / 2);


            if (ecartDate < diffDateFA && diffDateFA <= diffDateFC) {
                laPriorite = new DatabaseManager(context).LecturePrio(2);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));
            }
            if (diffDateFA <= ecartDate) {
                laPriorite = new DatabaseManager(context).LecturePrio(1);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));
            }
        }

        //Cas avec 1 prio
        if(idP == 1)
        {
            {
                laPriorite = new DatabaseManager(context).LecturePrio(1);
                imageView.setBackgroundColor(Color.parseColor(laPriorite.getImgP()));
            }
        }




        return view;
    }
}
